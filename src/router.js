import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Toolbar from './components/Toolbar.vue'
import Toolbar2 from './components/Toolbar2.vue'
import Grade from './views/Grade.vue'
import Restaurant from './views/Restaurant.vue'
import Dashboard from './views/Dashboard.vue'
import Movie from './views/HomeMovie.vue'
import movieDetail from './components/movieDetail.vue'
import addMovie from './components/AddMovie.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '',
      name: 'Toolbar2',
      component: Toolbar2,
      children: [
        {
          path: '/movie',
          name: 'movie',
          component: Movie
        },
        {
          path: 'movie/addMovie',
          name: 'addMovie',
          component: addMovie
        },
        {
          path: '/movie/movieDetail/',
          name: 'movieDetail',
          component: movieDetail
        }

      ]
    },
    {
      path: '/workshop',
      name: 'Toolbar',
      component: Toolbar,
      children: [
        {
          path: '/login',
          name: 'login',
          component: Login
        },
        {
          path: '/grade',
          name: 'grade',
          component: Grade
        },
        {
          path: '/restaurant',
          name: 'restaurant',
          component: Restaurant
        },
        {
          path: '/dashboard',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: '/about',
          name: 'about',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        }
      ]
    }
  ]
})
